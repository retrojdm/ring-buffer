#include <gtest/gtest.h>

#include "../src/RingBuffer.h"

class TestFixture : public testing::Test {
protected:
  // char tests
  static const size_t kCharBufferSize = 3;
  char charBuffer[kCharBufferSize];
  RingBuffer<char> charQueue;

  // int tests
  static const size_t kIntBufferSize = 10;
  int intBuffer[kIntBufferSize];
  RingBuffer<int> intQueue;

  TestFixture() :
    charQueue(charBuffer, kCharBufferSize),
    intQueue(intBuffer, kIntBufferSize)
  {
  }

  void SetUp() override {
    // Most of our removeIf() tests start with a full int queue.
    for (int i = 0; i < kIntBufferSize; i++)
    {
      intQueue.push(i);
    }
  }
};

TEST_F(TestFixture, RingBuffer_clear)
{
  charQueue.push('a');
  charQueue.push('b');
  charQueue.push('c');

  EXPECT_FALSE(charQueue.isEmpty());

  charQueue.clear();
  
  EXPECT_TRUE(charQueue.isEmpty());
}

TEST_F(TestFixture, RingBuffer_push)
{
  EXPECT_TRUE(charQueue.push('a'));
  EXPECT_TRUE(charQueue.push('b'));
  EXPECT_TRUE(charQueue.push('c'));
  EXPECT_FALSE(charQueue.push('d'));
}

// Note: it's the responsibility of the user to check that the ring buffer is not empty before calling .pop();
// Calling .pop() on an empty queue has undefined behaviour;
TEST_F(TestFixture, RingBuffer_pop)
{
  charQueue.push('a');
  charQueue.push('b');
  charQueue.push('c');
  
  EXPECT_FALSE(charQueue.isEmpty());
  char item = charQueue.pop();
  EXPECT_EQ(item, 'a');

  EXPECT_FALSE(charQueue.isEmpty());
  item = charQueue.pop();
  EXPECT_EQ(item, 'b');

  EXPECT_FALSE(charQueue.isEmpty());
  item = charQueue.pop();
  EXPECT_EQ(item, 'c');

  EXPECT_TRUE(charQueue.isEmpty());
}

TEST_F(TestFixture, RingBuffer_isEmpty)
{
  EXPECT_TRUE(charQueue.isEmpty());
  charQueue.push('a');

  EXPECT_FALSE(charQueue.isEmpty());
  char item = charQueue.pop();
  EXPECT_EQ(item, 'a');

  EXPECT_TRUE(charQueue.isEmpty());
}

TEST_F(TestFixture, RingBuffer_isFull)
{
  EXPECT_FALSE(charQueue.isFull());

  charQueue.push('a');

  EXPECT_FALSE(charQueue.isFull());

  charQueue.push('b');

  EXPECT_FALSE(charQueue.isFull());

  charQueue.push('c');

  EXPECT_TRUE(charQueue.isFull());

  EXPECT_FALSE(charQueue.isEmpty());
  charQueue.pop();

  EXPECT_FALSE(charQueue.isFull());
}

TEST_F(TestFixture, RingBuffer_count)
{
  EXPECT_EQ(charQueue.count(), 0);

  charQueue.push('a');

  EXPECT_EQ(charQueue.count(), 1);

  charQueue.push('b');

  EXPECT_EQ(charQueue.count(), 2);

  charQueue.push('c');

  EXPECT_EQ(charQueue.count(), 3);

  EXPECT_FALSE(charQueue.isEmpty());
  charQueue.pop();

  EXPECT_EQ(charQueue.count(), 2);

  EXPECT_FALSE(charQueue.isEmpty());
  charQueue.pop();

  EXPECT_EQ(charQueue.count(), 1);

  EXPECT_FALSE(charQueue.isEmpty());
  charQueue.pop();

  EXPECT_EQ(charQueue.count(), 0);
}

TEST_F(TestFixture, RingBuffer_removeIf_odd)
{
  EXPECT_EQ(intQueue.count(), 10);

  // Remove all odd items.
  intQueue.removeIf([](const int& item) { return item % 2 == 1; });

  EXPECT_EQ(intQueue.count(), 5);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 0);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 2);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 4);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 6);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 8);

  EXPECT_TRUE(intQueue.isEmpty());
}

TEST_F(TestFixture, RingBuffer_removeIf_lessThanFive)
{
  EXPECT_EQ(intQueue.count(), 10);

  // Remove lower half.
  intQueue.removeIf([](const int& item) { return item < 5; });

  EXPECT_EQ(intQueue.count(), 5);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 5);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 6);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 7);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 8);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 9);

  EXPECT_TRUE(intQueue.isEmpty());
}

TEST_F(TestFixture, RingBuffer_removeIf_greaterThanFour)
{
  EXPECT_EQ(intQueue.count(), 10);

  // Remove top half.
  intQueue.removeIf([](const int& item) { return item >= 5; });

  EXPECT_EQ(intQueue.count(), 5);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 0);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 1);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 2);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 3);

  EXPECT_FALSE(intQueue.isEmpty());
  EXPECT_EQ(intQueue.pop(), 4);

  EXPECT_TRUE(intQueue.isEmpty());
}

TEST_F(TestFixture, RingBuffer_removeIf_pointers)
{
  // Pointer tests
  static const size_t kPointerBufferSize = 3;
  int* pointerBuffer[kPointerBufferSize];
  RingBuffer<int*> pointerQueue(pointerBuffer, kPointerBufferSize);

  // A very crude way to keep track of the total number of items deleted.
  // This must be static, since we can't pass state to lambdas via capturing, since they're actually function pointers.
  // A more robust way would be to log deleted items in another ring-buffer, but that sounds like a lot of work ;)
  static int deleted = 0;

  int *item1 = new int(1);
  int *item2 = new int(2);
  int *item3 = new int(3);

  pointerQueue.push(item1);
  pointerQueue.push(item2);
  pointerQueue.push(item3);

  // Remove top half.
  pointerQueue.removeIf(
    [](int* const & item) { return *item == 2; }, // bool condition(T)
    [](int* const & item) { delete item; deleted++; }); // void onDelete(T)

  EXPECT_EQ(pointerQueue.count(), 2);
  EXPECT_EQ(deleted, 1);


  EXPECT_FALSE(intQueue.isEmpty());
  int *item = pointerQueue.pop();
  EXPECT_EQ(*item, 1);

  EXPECT_FALSE(intQueue.isEmpty());
  item = pointerQueue.pop();
  EXPECT_EQ(*item, 3);
  
  delete item1;
  delete item3;
}