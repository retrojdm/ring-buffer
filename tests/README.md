# Unit tests 

The main advantage of unit tests in an Arduino project is to speed up development. They allow small pieces of code to be tested directly on a PC instead of a microcontroller - avoiding a full build and upload cycle for every little change.

In this project, the unit tests use the [GoogleTest](http://google.github.io/googletest/quickstart-cmake.html) library and run from a terminal (eg: [git for windows](https://gitforwindows.org/)).

For this to work we only test non Arduino-specific code. Eg: No `digitalWrite()` or `Serial.print()` etc.

The `/src/Platform.h` file contains a compiler directive that substitutes `<Arduino.h>` for some standard libraries, allowing for compatibility with the C++ compiler used by these tests.

Before you can run them you'll need to install:

- [CMake](https://cmake.org/) (cross-platform build-process manager),
- [Ninja](https://ninja-build.org/) (build generator), and
- [Clang](https://www.wikihow.com/Install-Clang-on-Windows) (C++ compiler, only needed on Windows)

## Note for windows users

If you use clang, you'll have to modify the `CMAKE_C_COMPILER` and `CMAKE_CXX_COMPILER` variables in [CMakeLists.txt](./CMakeLists.txt) to `clang` and `clang++`.

You'll need to also add their locations to your `PATH` environment variable. Eg:

```
C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Tools\Llvm\bin
C:\Program Files\CMake\bin
C:\Program Files\Ninja
```

## Building

After everything's set up, you should be able to build your test environment from a terminal:

```bash
cd test
./build.sh
```

You should see something like:

![GTest Arduino Unit Tests](./screenshots/gtest-ardunio-unit-tests--build.png)

## Running

Then, whenever you make changes to your code you only need to run the tests with:

```bash
./run.sh
```

You should see something like:

![GTest Arduino Unit Tests](./screenshots/gtest-ardunio-unit-tests--run.png)
