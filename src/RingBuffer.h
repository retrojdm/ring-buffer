#pragma once

#if defined(ARDUINO)
#include <Arduino.h>
#else
#include <stdint.h>
#include <stddef.h> // for size_t
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Class definition

template<typename T>
class RingBuffer {
public:
  RingBuffer (T *buffer, size_t bufferSize);
  
  void clear();
  bool push (const T item);
  T pop ();
  bool isEmpty () const;
  bool isFull () const;
  size_t count () const;
  void removeIf(bool (*condition)(const T&));
  void removeIf(bool (*condition)(const T&), void (*onDelete)(const T&));

private:
  T *_contents;
  size_t _maxSize;
  size_t _items;
  uint16_t _head;
  uint16_t _tail;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Public functions
//
// Since this is a template class, the implementation is here in the header file (to avoid "Undefined reference to"
// errors).

// Class constructor
template<typename T>
RingBuffer<T>::RingBuffer(T *buffer, size_t bufferSize)
{
  _maxSize = bufferSize;
  _contents = buffer;
  clear();
}

// T clear()
// Don't use this with pointer types.
template<typename T>
void RingBuffer<T>::clear()
{
  _items = 0;
  _head = 0;
  _tail = 0;
}

// bool push(T)
template<typename T>
bool RingBuffer<T>::push (const T item)
{
  if (isFull())
  {
    return false;
  }

  _contents[_tail++] = item;
  if (_tail == _maxSize) _tail = 0;
  _items++;

  return true;
}

// T pop()
//
// IMPORTANT:
// Always check that the ring buffer is not empty before calling .pop();
template<typename T>
T RingBuffer<T>::pop()
{
  T item = _contents[_head++];
  _items--;
  if (_head == _maxSize) _head = 0;
  return item;
}

// bool isEmpty()
template<typename T>
bool RingBuffer<T>::isEmpty() const
{
  return _items == 0;
}

// bool isFull()
template<typename T>
bool RingBuffer<T>::isFull() const
{
  return _items == _maxSize;
}

// uint16_t count()
template<typename T>
size_t RingBuffer<T>::count () const
{
  return _items;
}

template<typename T>
void RingBuffer<T>::removeIf(bool (*predicate)(const T&))
{
  removeIf(predicate, nullptr);
}

// void removeIf()
// Removes all items that match the condition specified by the predicate.
// Use the `onDelete()' callback for cleanup like deallocating pointers.
template<typename T>
void RingBuffer<T>::removeIf(bool (*condition)(const T&), void (*onDelete)(const T&))
{
  if (isEmpty()) {
      return;
  }

  uint16_t readIndex = _head;  // Start reading from the head.
  uint16_t writeIndex = _head; // Start writing from the head.
  size_t newCount = 0;         // Count of retained items.

  for (size_t i = 0; i < _items; ++i)
  {
    const T& currentItem = _contents[readIndex];

    if (!condition(currentItem))
    {
      // Retain this item.
      _contents[writeIndex] = currentItem;
      writeIndex = (writeIndex + 1) % _maxSize;
      newCount++;
    }
    else if (onDelete)
    {
      onDelete(currentItem);
    }

    // Move to the next item.
    readIndex = (readIndex + 1) % _maxSize;
  }

  // Update the ring buffer state.
  _tail = writeIndex; // Tail is the next write position.
  _items = newCount;  // Update the item count.

  // If no items remain, reset head and tail.
  if (newCount == 0)
  {
    _head = 0;
    _tail = 0;
  }
}
