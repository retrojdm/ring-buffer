#include <RingBuffer.h>

const int kBufferSize = 3;
int gBuffer[kBufferSize];

RingBuffer<int> gQueue(gBuffer, kBufferSize);


void setup() {
  Serial.begin(9600);
  Serial.println("RingBuffer simple queue example");

  Serial.println("Before starting...");
  printStatus();
  
  Serial.println("Pushing...");
  for (int i = 0; i < kBufferSize; i++)
  {
    if (gQueue.push(i))
    {
      Serial.print("Pushed ");
      Serial.println(i);
    }
    else
    {
      Serial.print("Couldn't push ");
      Serial.print(i);
    }
    printStatus();    
  }

  Serial.println("Popping...");
  for (int i = 0; i < kBufferSize; i++)
  {
    if (gQueue.isEmpty())
    {
      Serial.print("Can't pop.");
    }
    else
    {
      int value = gQueue.pop();
      Serial.print("Popped ");
      Serial.println(value);
    }
    printStatus();
  }

  Serial.println("All done!");
}


void loop() {
  
}


void printStatus()
{
  Serial.print("gQueue.isEmpty():");
  Serial.println(gQueue.isEmpty());
  Serial.print("gQueue.isFull():");
  Serial.println(gQueue.isFull());  
  Serial.print("gQueue.count():");
  Serial.println(gQueue.count());
}